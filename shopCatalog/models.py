# -*- coding: utf-8 -*-
# Create your models here.
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from django.db import models


def validate_positive(value):
    if value < 0:
        raise ValidationError(u'Grade must be greater than 0')


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Good(models.Model):
    name = models.CharField(max_length=150)
    category = models.ForeignKey(Category)
    longName = models.CharField(max_length=300)
    price = models.FloatField(validators=[validate_positive])
    description = models.TextField()
    imagePath = models.CharField(max_length=400)
    isAvailable = models.BooleanField()
    image = models.ImageField(upload_to='images/', null= True)


class Like(models.Model):
    item = models.ForeignKey(Good)
    user = models.ForeignKey(User)


class Comment(models.Model):
    item = models.ForeignKey(Good)
    user = models.ForeignKey(User)
    text = models.TextField()
    date = models.DateTimeField()


# коммент отличается от ревью тем, что
# авторизированный пользователь может оставить еще и оценку к товару,
# что и будет считаться отзывом, а не комментом
class Review(Comment):
    rate = models.IntegerField()


class News(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    date = models.DateField()


# храним скидку для данного города,
# что-то типа доп коэфициента
class City(models.Model):
    name = models.CharField(max_length=20)
    discount = models.FloatField()


# храним в бд, чтобы в случае выхода восстановить из бд
# будем хранить в случае незавершенного заказа
class Basket(models.Model):
    user = models.ForeignKey(User)
    items = models.ManyToManyField(Good)


class Order(models.Model):
    user = models.ForeignKey(User)
    city = models.ForeignKey(City, null=True)
    items = models.ManyToManyField(Good)
    totalPrice = models.FloatField(default=0.0)
    is_purchased = models.BooleanField(default=False)
    address = models.CharField(max_length=500)
    payment_method = models.CharField(max_length=100)


# акции к определенной категории товаров
class Action(models.Model):
    discount = models.FloatField()
    expireDate = models.DateField()
    action_url = models.URLField()
    imageUrl = models.CharField(max_length=400)





