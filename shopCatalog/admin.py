from django.contrib import admin

from shopCatalog.models import Good, Action, Category, City, Comment, Order, Like, Review


class GoodAdmin(admin.ModelAdmin):
    list_display = ('name', 'longName',)


class ActionAdmin(admin.ModelAdmin):
    list_display = ('discount', 'expireDate')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)


class CityAdmin(admin.ModelAdmin):
    list_display = ('name',)


class CommentAdmin(admin.ModelAdmin):
    list_display = ('item', 'user', 'text')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('items_ids', 'user', 'city', 'totalPrice', 'is_purchased')

    def items_ids(self, obj):
        string = ''
        for item in obj.items.all():
            string += item.name + ', '
        return string

    items_ids.short_description = "Items"


class LikeAdmin(admin.ModelAdmin):
    list_display = ('item', 'user')

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('rate',)


admin.site.register(Good, GoodAdmin)
admin.site.register(Action, ActionAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Like, LikeAdmin)
admin.site.register(Review, ReviewAdmin)
