from django.forms.models import ModelForm

from shopCatalog.models import Good, Review


class GoodForm(ModelForm):
    class Meta:
        model = Good
        error_messages = {
            'name': {
                'max_length': "This good's name is too long."
            },
        }


class ReviewForm(ModelForm):
    class Meta:
        model = Review
        error_messages = {

        }
        exclude = ('user', 'item', 'date')