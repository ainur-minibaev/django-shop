# Create your views here.
import sys
import datetime

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Min, Max
from django.db.models.aggregates import Count
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy

from shopCatalog.forms import GoodForm, ReviewForm
from shopCatalog.models import Good, Order, City, Action, Like, Review


def index(request):
    param_map = {}
    goods_ids = Order.objects.values_list("items")
    # get best sold product and if in list of best
    # less than 6 goods append to them random goods
    # some kind of magic
    ids_count = {}
    for order_goods in goods_ids:
        for id in list(order_goods):
            if ids_count.has_key(id):
                ids_count[id] += 1
            else:
                ids_count[id] = 1
    id_keys_len = len(ids_count.keys())
    if id_keys_len > 6:
        sorted_ids = sorted(ids_count, ids_count.__getitem__)[0:5]
        goods = list(Good.objects.filter(id__in=sorted_ids))
    else:
        goods = list(Good.objects.filter(id__in=ids_count.keys()))
        last_object_count = 6 - id_keys_len
        if last_object_count < len(Good.objects.all()):
            goods += list(Good.objects.exclude(id__in=ids_count.keys()).order_by('?')[0:last_object_count])
    param_map["goods"] = goods
    now = datetime.datetime.now()
    param_map["actions"] = Action.objects.filter(expireDate__gte=now.strftime("%Y-%m-%d"))
    param_map["reviews"] = Review.objects.all()
    return render(request, "mainPage.html", param_map)


def addGood(request):
    param_map = {}
    if request.method == "POST":
        good_form = GoodForm(request.POST, request.FILES)
        if good_form.is_valid():
            good_entity = good_form.save()
            return HttpResponseRedirect(reverse("good_page", kwargs={'good_id': good_entity.id}))
        else:
            param_map["good_form"] = good_form
            return render(request, "shopCatalog/addGood.html", param_map)

    elif request.method == "GET":
        param_map["good_form"] = GoodForm()
        return render(request, "shopCatalog/addGood.html", param_map)


def render_good(request, good_id):
    param_map = {}
    good_id = int(good_id)
    try:
        city = City.objects.get(id=request.COOKIES.get('city', '-100'))
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse("index"));
    try:
        good = Good.objects.get(id=good_id)
        good.price *= city.discount
    except ObjectDoesNotExist:
        good = None
    if good is None:
        return render(request, "errors/404.html")
    watched_goods = request.session.get("watched_goods", [])
    watched_goods.append(good.id)
    request.session["watched_goods"] = watched_goods
    param_map["good"] = good

    if Like.objects.filter(user__id=request.user.id, item__id=good_id):
        param_map["is_liked"] = True
    param_map["likes_count"] = Like.objects.filter(item__id=good_id).aggregate(Count('item'))['item__count']
    try:
        order = Order.objects.get(is_purchased=False, user__id=request.user.id)
        param_map["in_cart"] = good in order.items.all()
    except ObjectDoesNotExist:
        pass

    param_map["review_form"] = ReviewForm()
    param_map["reviews"] = Review.objects.filter(item__id=good_id)
    return render(request, "shopCatalog/one_good.html", param_map)


@login_required(login_url=reverse_lazy('login'))
def add_to_cart(request, good_id):
    try:
        good = Good.objects.get(id=int(good_id))
    except ObjectDoesNotExist:
        good = None
    if good is None or not good.isAvailable:
        return render(request, "errors/404.html")
    # find unclosed order
    try:
        order = Order.objects.get(is_purchased=False, user__id=request.user.id)
    except ObjectDoesNotExist:
        order = Order()
        order.user = request.user
        order.save()
    # TODO calc discount
    order.items.add(good)
    order.totalPrice += good.price
    order.save()
    return HttpResponseRedirect(reverse('cart'))


@login_required(login_url=reverse_lazy('login'))
def render_cart(request):
    param_map = {}
    try:
        order = Order.objects.get(is_purchased=False, user__id=request.user.id)
    except ObjectDoesNotExist:
        order = None

    param_map["order"] = order
    return render(request, "shopCatalog/cart.html", param_map)


@login_required(login_url=reverse_lazy('login'))
def delete_from_cart(request, good_id):
    good_id = int(good_id)
    try:
        order = Order.objects.get(is_purchased=False, user__id=request.user.id)
    except ObjectDoesNotExist:
        order = None
    if order:
        order.items = filter(lambda item: item.id != good_id, order.items.all())
        order.save()
    return HttpResponseRedirect(reverse('cart'))


def render_make_order(request):
    param_map = {}
    try:
        order = Order.objects.get(is_purchased=False, user__id=request.user.id)
    except ObjectDoesNotExist:
        order = None
    if (order is None):
        return HttpResponseRedirect(reverse('index'))

    param_map["order"] = order

    return render(request, "shopCatalog/make_order.html", param_map)


def make_order(request):
    if (not request.method == 'POST'):
        return HttpResponseRedirect(reverse('index'))

    param_map = {}
    try:
        order = Order.objects.get(is_purchased=False, user__id=request.user.id)
    except ObjectDoesNotExist:
        order = None
    if (order is None):
        return HttpResponseRedirect(reverse('index'))

    address = request.POST["address"]
    payment = request.POST["payment"]
    order.address = address
    order.payment_method = payment
    order.is_purchased = True
    order.save()
    param_map["order"] = order

    return render(request, "shopCatalog/success_order.html", param_map)


def filter_catalog(request):
    param_map = {}
    if (request.method == 'POST'):
        request_map = request.POST
        html_view_url = "shopCatalog/catalog_part.html"
    else:
        request_map = request.GET
        param_map["min_price"] = int(Good.objects.aggregate(Min('price'))['price__min']) - 1
        param_map["max_price"] = int(Good.objects.aggregate(Max('price'))['price__max']) + 1
        param_map["c_price_l"] = param_map["min_price"]
        param_map["c_price_r"] = param_map["max_price"]
        html_view_url = "shopCatalog/catalog.html"

    try:
        category_ = int(unicode(request_map.get("catalog", None)))
    except ValueError:
        category_ = None
    try:
        min_price = int(unicode(request_map.get("min_price", -sys.maxsize - 1)))
        if request.method == 'GET' and min_price > param_map["min_price"]:
            param_map["c_price_l"] = min_price
    except ValueError:
        min_price = -sys.maxsize - 1

    try:
        max_price = int(unicode(request_map.get("max_price", sys.maxsize)))
        if request.method == 'GET' and max_price < param_map["max_price"]:
            param_map["c_price_r"] = max_price
    except ValueError:
        max_price = sys.maxsize
    try:
        page = int(unicode(request_map.get("page", 0)))
    except ValueError:
        page = 0
    if category_ and min_price:
        goods = Good.objects.filter(category__id=category_, price__range=(min_price, max_price)).order_by("id")[
                page * 3:(page + 1) * 3]
    else:
        if category_ and not min_price:
            goods = Good.objects.filter(category__id=category_).order_by("id")[page * 3:(page + 1) * 3]
        else:
            goods = Good.objects.filter(price__range=(min_price, max_price)).order_by("id")[page * 3:(page + 1) * 3]

    param_map["good_list"] = goods
    return render(request, html_view_url, param_map)
