$(document).ready(function () {
    $("a[data-auth]").on('click', function (e) {
        window.location = $(this).data('auth')
    });

    $("#like-btn").click(function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            type: "POST",
            url: btn.attr('href'),
            data: {csrfmiddlewaretoken: csrf_token},
            success: function () {
                $("#like-btn").toggleClass('active');
                var count = parseInt($("#like-count").html());
                if ($("#like-btn").hasClass('active')) {
                    count++;
                } else {
                    count--;
                }
                if (count == 0) {
                    $("#like-p").addClass("hidden")
                } else {
                    $("#like-p").removeClass("hidden")
                }
                $("#like-count").html(count);
            }
        });
        return false
    });

});